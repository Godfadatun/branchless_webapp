// import something here
// import Vue from 'vue';
import VueSocialSharing  from 'vue-social-sharing';
// "async" is optional
export default async ({Vue}) => {
  // something to do
  Vue.use(VueSocialSharing);
}
