/*
export function someMutation (state) {
}
*/
import { MutationTree } from 'vuex';
import { stateModel } from './state';
import { LocalStorage } from 'quasar'
const mutation: MutationTree<stateModel> = {
  user(state: stateModel, data){
    console.log('data: ', data)
    state.user = data
  },
  banks(state: stateModel, data){
    state.banks = data
  },

  states(state: stateModel, data){
    state.states = data
  },

  transaction(state: stateModel, data){
    state.transaction = data
  },

  balance(state: stateModel, data){
    state.balance = data
  },
  
  agent_email(state: stateModel, data){
    state.agent_email = data
  },

  savings(state: stateModel, data){
    state.savings = data
  },

  agents(state: stateModel, data){
    state.agents = data
  },

  savings_plans(state: stateModel, data){
    state.savings_plans = data
  },

  login(state: stateModel, data){
    LocalStorage.set('branchless_token', data)
    state.token = data
  },

  logout(state: stateModel){
    LocalStorage.set('branchless_token', '');
    LocalStorage.set('branchless_is_agent', false);
    LocalStorage.set('branchless_is_subscribed', false);
    LocalStorage.set('branchless_sub_status', false);
    state.user = [];
    state.banks = [];
    state.token = '';
    state.savings = [];
    state.transaction = [];
    state.is_agent = false;
    state.is_subscribed = false;
    state.sub_status = false;
  },
  is_agent(state: stateModel, data){
    state.is_agent = data
  },
  is_subscribed(state: stateModel, data){
    state.sub_status = data
  },

  sub_status(state: stateModel, data){
    console.log('mutated_status', data)
    state.sub_status = data
    console.log('mutated_status_state', state.sub_status )
  },

  agentPlans(state: stateModel, data){
    console.log('the data 1',data);
    let plans:Array<any> = []
    data.forEach((element:any) => {

      if(element.duration == 0 && element.name != 'agent' && element.status != 'cancelled'  ) {
        // state.agentPlans.push(element)
        plans.push(element)
      }
    });
    state.agentPlans = plans
  }
};

export default mutation;
