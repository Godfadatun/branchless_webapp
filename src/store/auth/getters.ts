// sends info from the state to the APP
import { GetterTree } from 'vuex';
import { StoreInterface } from '../index';
import { stateModel } from './state';
import { LocalStorage } from 'quasar';

const getters: GetterTree<stateModel, StoreInterface> = {
  users (state) { return state.user },
  banks (state) { return state.banks },
  states (state) { return state.states },
  savings (state) { return state.savings },
  savings_plans (state) { return state.savings_plans },
  transaction (state) { return state.transaction },
  is_agent(state){ return state.is_agent || LocalStorage.getItem('branchless_is_agent') },
  is_subscribed(state){ return state.is_subscribed || LocalStorage.getItem('branchless_is_subscribed') },
  isLoggedIn (state) { return state.token || LocalStorage.getItem('branchless_token') },
  agentPlans (state) { return state.agentPlans },
  sub_status(state){ return state.sub_status },
  balance(state){ return state.balance },
  agent_email(state){ return state.agent_email },
};

export default getters;
