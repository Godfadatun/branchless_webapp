import { LocalStorage } from 'quasar'

export interface stateModel {
  user: Array<any>;
  banks: Array<any>;
  token: string;
  balance: string;
  states: Array<any>;
  savings: Array<any>;
  transaction: any;
  is_agent: Boolean;
  is_subscribed: Boolean;
  savings_plans: Array<any>;
  agentPlans: Array<any>;
  sub_status: Boolean;
  agents: any;
  agent_email: string;
}

const state: stateModel ={
  user: [],
  banks: [],
  token: LocalStorage.getItem('branchless_token')||'',
  balance: '',
  states : [
    "Abia", "Adamawa", "Akwa Ibom", "Anambra", "Bauchi", "Bayelsa", "Benue", "Borno", "Cross River", "Delta", "Ebonyi", "Edo", "Ekiti", "Enugu", "FCT - Abuja", "Gombe", "Imo", "Jigawa", "Kaduna", "Kano", "Katsina", "Kebbi", "Kogi", "Kwara", "Lagos", "Nasarawa", "Niger", "Ogun", "Ondo", "Osun", "Oyo", "Plateau", "Rivers", "Sokoto", "Taraba", "Yobe", "Zamfara"
  ],
  transaction:[],
  savings:[],
  is_agent: false,
  is_subscribed: false,
  savings_plans: [],
  agentPlans:[],
  sub_status: false,
  agents:null,
  agent_email: ''
}

export default state
