/*
export function someAction (context) {
}
*/

import { ActionTree } from 'vuex';
import { StoreInterface } from '../index';
import { stateModel } from './state';
import axios from 'axios';
import { promises } from 'dns';
import { LocalStorage } from 'quasar'
// import router from '../../App.vue'
// import { router } from 'quasar';

const actions: ActionTree<stateModel, StoreInterface> = {

  async thebanklist({ commit }){
    axios
    .get('https://branchless.world/user/getBanks')
      .then(response => {
        commit('banks', response.data.data)
      })
    .catch(err=>{
      console.log('err: ', err)
    })
  },

  async thestates({ commit }){
    axios
    .get('http://locationsng-api.herokuapp.com/api/v1/cities')
    .then(response =>{
      commit('states', response.data)
    })
  },

  async getUser({ commit }){
    axios.get('https://branchless.world/user/userDetails')
     .then(response => {
      commit('user', response.data.data)
    })
    .catch(error=>{
      if (error.response) {
        // console.log(error.response.data.message);

        if(error.response.data.message == 'jwt expired'){
          commit('logout')
        }
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
    })
  },

  async getSavings({ commit }){
    axios.get('https://branchless.world/user/getSavings')
     .then(response => {
       console.log(response.data.data)
      commit('savings', response.data.data)

    })
    .catch(err=>{
      console.log('err: ', err)
    })
  },

  async getSavingsPlans({ commit }){
    axios.get('https://branchless.world/user/getPlans')
    .then(response => {
      commit('savings_plans', response.data.data)
      commit('agentPlans', response.data.data)
    })
    .catch(err=>{
      console.log('err: ', err)
    })
  },

  async getBalance({ commit }){
    axios.get('https://branchless.world/user/walletBalance')

      .then(response => {
        commit('balance', response.data.data.balance/100)
        // this.balance = response.data.data.balance/100
      })
      .catch(error=>{
        console.log('err: ', error)
      })
  },

  
}


export default actions;

