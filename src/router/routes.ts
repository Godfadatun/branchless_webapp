import { RouteConfig } from 'vue-router';

const routes: RouteConfig[] = [
  {
    path: '',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'login',component: () => import('pages/Auth.vue') },
      { path: 'register', name: 'register',component: () => import('pages/Auth.vue'), props: true },
      { path: 'referrer/:item', name: 'referrer',component: () => import('pages/Auth.vue') },
      { path: 'forgot', name: 'forgot',component: () => import('pages/Auth.vue') },
      { path: 'account', name: 'account',component: () => import('pages/Auth.vue')},
      { path: 'otp/:item', name: 'otp',component: () => import('pages/Auth.vue') },
      { path: 'funds/:item', name: 'funds',component: () => import('pages/Auth.vue') },
      { path: 'agentsub', name: 'agentsub',component: () => import('pages/Auth.vue') },
      { path: 'initAccount', name: 'initAccount',component: () => import('pages/Auth.vue') },
      { path: 'dashboard', name: 'dashboard',component: () => import('pages/Dashboard.vue'), meta: { requiresAuth: true }},
      { path: 'changepin', name: 'changepin',component: () => import('pages/Dashboard.vue'), meta: { requiresAuth: true } },
      { path: 'settings', name: 'settings',component: () => import('pages/Dashboard.vue'), meta: { requiresAuth: true } },
      { path: 'transaction/:item', name: 'transaction',component: () => import('pages/Transaction.vue'), props: true, meta: { requiresAuth: true } },
    ]
  },

];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  });
}

export default routes;
