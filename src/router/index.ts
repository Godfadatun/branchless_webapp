import { route } from 'quasar/wrappers';
import VueRouter from 'vue-router';
import { StoreInterface } from '../store';
import routes from './routes';
import { LocalStorage } from 'quasar'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default route<StoreInterface>(function ({ Vue }) {
  Vue.use(VueRouter);

  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,
    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  Router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
      if (LocalStorage.getItem('branchless_token')) {
        if (LocalStorage.getItem('branchless_sub_status')) {
          if(LocalStorage.getItem('branchless_is_agent')){
            if (LocalStorage.getItem('branchless_is_subscribed')) {
              next()
              return
            } else {
              next({name: 'agentsub'})
              // next()
            }
          }else{
            next()
            return
          }
        } else {
          next({name: 'initAccount'})
        }
      }else{
        next({name: 'login'})
      }
    } else {
      next()
    }
  })

  Router.beforeEach((to, from, next)=>{
    console.log(to.name);
    if((to.name == 'account' || to.name == 'agentsub' || to.name == 'initAccount' ) && from.name == null) next({name: 'login'})
    else if ((to.name == 'account' || to.name == 'agentsub' || to.name == 'initAccount' ) && from.name == 'login') next()
    // else if ((to.name == 'account' || to.name == 'agentsub' || to.name == 'initAccount' ) && from.name != 'login') next(false)
    // else (to.name == 'account' || to.name == 'agentsub') ? next(false) : next()
    else next()
  })

  // Router.beforeEach((to, from, next) => {
  //   if (to.name == 'account' && (from.name == 'otp' || from.name == 'login' )) next()
  //   // else if (to.name == 'login' && (from.name == 'dashboard'||from.name == 'changepin' ||from.name == 'settings'||from.name == 'transaction')) next()
  //   else if((to.name == 'account' || to.name == 'agentsub' ) && from.name == null) next({name: 'login'})
  //   else (to.name == 'account' || to.name == 'agentsub') ? next(false) : next()
  // })

  return Router;
})
